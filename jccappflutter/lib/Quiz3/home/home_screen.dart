import 'package:flutter/material.dart';

import 'package:jccappflutter/Quiz3/home/model.dart';

class Homescreen extends StatefulWidget {
  String? user;
  Homescreen({Key? key, required this.user}) : super(key: key);

  @override
  _HomescreenState createState() => _HomescreenState(user);
}

class _HomescreenState extends State<Homescreen> {
  String? user;
  _HomescreenState(this.user);
  @override
  void initState() {
    super.initState();
  }

  int _harga = 0;
  void _tambahhargakmp() {
    setState(() {
      _harga += 20000000;
    });
  }

  void _tambahhargaimc() {
    setState(() {
      _harga += 25000000;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.all(16),
        margin: EdgeInsets.only(top: 20),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(40.0),
                      child: Image.network(
                        "https://avatars.githubusercontent.com/u/52710807?v=4",
                        height: 80,
                        width: 80,
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(user!, style: TextStyle(fontSize: 20),),
                  ],
                ),
                Row(
                  children: [
                    //## soal 4
                    //Tulis Coding disini
                    Text("Rp ${_harga}", style: TextStyle(fontSize: 14),),

                    //sampai disini
                    SizedBox(
                      width: 10,
                    ),
                    Icon(Icons.add_shopping_cart)
                  ],
                ),
              ],
            ),
            SizedBox(height: 40),
            SizedBox(
              height: 2000,
              child: GridView.count(
                crossAxisCount: 2,
                childAspectRatio: 1.491,
                crossAxisSpacing: 21,
                mainAxisSpacing: 14,
                children: [
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(9),
                      color: Colors.black,
                    ),
                    child: Container(
                      margin: EdgeInsets.all(8),
                      decoration: BoxDecoration(color: Colors.blueGrey[300]),
                      child: Column(
                        children: [
                          Container(
                              height: 60,
                              width: 60,
                              child: SizedBox(
                                  child: Image.network(items[0].profileUrl))),
                          Text(items[0].name),
                          Text(items[0].price.toString()),
                          SizedBox(
                            height: 8,
                          ),
                          Material(
                            elevation: 5.0,
                            color: Colors.blue.shade200,
                            child: MaterialButton(
                              minWidth: 20,
                              padding: EdgeInsets.only(
                                  top: 10, bottom: 10, left: 30, right: 30),
                              onPressed: _tambahhargakmp,
                              child: Text(
                                "Beli",
                                textAlign: TextAlign.center,
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(9),
                      color: Colors.black,
                    ),
                    child: Container(
                      margin: EdgeInsets.all(8),
                      decoration: BoxDecoration(color: Colors.blueGrey[300]),
                      child: Column(
                        children: [
                          Container(
                              height: 60,
                              width: 60,
                              child: SizedBox(
                                  child: Image.network(items[1].profileUrl))),
                          Text(items[1].name),
                          Text(items[1].price.toString()),
                          SizedBox(
                            height: 8,
                          ),
                          Material(
                            elevation: 5.0,
                            color: Colors.blue.shade200,
                            child: MaterialButton(
                              minWidth: 20,
                              padding: EdgeInsets.only(
                                  top: 10, bottom: 10, left: 30, right: 30),
                              onPressed: _tambahhargaimc,
                              child: Text(
                                "Beli",
                                textAlign: TextAlign.center,
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(9),
                      color: Colors.black,
                    ),
                    child: Container(
                      margin: EdgeInsets.all(8),
                      decoration: BoxDecoration(color: Colors.blueGrey[300]),
                      child: Column(
                        children: [
                          Container(
                              height: 60,
                              width: 60,
                              child: SizedBox(
                                  child: Image.network(items[2].profileUrl))),
                          Text(items[2].name),
                          Text(items[2].price.toString()),
                          SizedBox(
                            height: 8,
                          ),
                          Material(
                            elevation: 5.0,
                            color: Colors.blue.shade200,
                            child: MaterialButton(
                              minWidth: 20,
                              padding: EdgeInsets.only(
                                  top: 10, bottom: 10, left: 30, right: 30),
                              onPressed: _tambahhargakmp,
                              child: Text(
                                "Beli",
                                textAlign: TextAlign.center,
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(9),
                      color: Colors.black,
                    ),
                    child: Container(
                      margin: EdgeInsets.all(8),
                      decoration: BoxDecoration(color: Colors.blueGrey[300]),
                      child: Column(
                        children: [
                          Container(
                              height: 60,
                              width: 60,
                              child: SizedBox(
                                  child: Image.network(items[3].profileUrl))),
                          Text(items[3].name),
                          Text(items[3].price.toString()),
                          SizedBox(
                            height: 8,
                          ),
                          Material(
                            elevation: 5.0,
                            color: Colors.blue.shade200,
                            child: MaterialButton(
                              minWidth: 20,
                              padding: EdgeInsets.only(
                                  top: 10, bottom: 10, left: 30, right: 30),
                              onPressed: _tambahhargaimc,
                              child: Text(
                                "Beli",
                                textAlign: TextAlign.center,
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(9),
                      color: Colors.black,
                    ),
                    child: Container(
                      margin: EdgeInsets.all(8),
                      decoration: BoxDecoration(color: Colors.blueGrey[300]),
                      child: Column(
                        children: [
                          Container(
                              height: 60,
                              width: 60,
                              child: SizedBox(
                                  child: Image.network(items[4].profileUrl))),
                          Text(items[4].name),
                          Text(items[4].price.toString()),
                          SizedBox(
                            height: 8,
                          ),
                          Material(
                            elevation: 5.0,
                            color: Colors.blue.shade200,
                            child: MaterialButton(
                              minWidth: 20,
                              padding: EdgeInsets.only(
                                  top: 10, bottom: 10, left: 30, right: 30),
                              onPressed: _tambahhargakmp,
                              child: Text(
                                "Beli",
                                textAlign: TextAlign.center,
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(9),
                      color: Colors.black,
                    ),
                    child: Container(
                      margin: EdgeInsets.all(8),
                      decoration: BoxDecoration(color: Colors.blueGrey[300]),
                      child: Column(
                        children: [
                          Container(
                              height: 60,
                              width: 60,
                              child: SizedBox(
                                  child: Image.network(items[5].profileUrl))),
                          Text(items[5].name),
                          Text(items[5].price.toString()),
                          SizedBox(
                            height: 8,
                          ),
                          Material(
                            elevation: 5.0,
                            color: Colors.blue.shade200,
                            child: MaterialButton(
                              minWidth: 20,
                              padding: EdgeInsets.only(
                                  top: 10, bottom: 10, left: 30, right: 30),
                              onPressed: _tambahhargaimc,
                              child: Text(
                                "Beli",
                                textAlign: TextAlign.center,
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(9),
                      color: Colors.black,
                    ),
                    child: Container(
                      margin: EdgeInsets.all(8),
                      decoration: BoxDecoration(color: Colors.blueGrey[300]),
                      child: Column(
                        children: [
                          Container(
                              height: 60,
                              width: 60,
                              child: SizedBox(
                                  child: Image.network(items[6].profileUrl))),
                          Text(items[6].name),
                          Text(items[6].price.toString()),
                          SizedBox(
                            height: 8,
                          ),
                          Material(
                            elevation: 5.0,
                            color: Colors.blue.shade200,
                            child: MaterialButton(
                              minWidth: 20,
                              padding: EdgeInsets.only(
                                  top: 10, bottom: 10, left: 30, right: 30),
                              onPressed: _tambahhargakmp,
                              child: Text(
                                "Beli",
                                textAlign: TextAlign.center,
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(9),
                      color: Colors.black,
                    ),
                    child: Container(
                      margin: EdgeInsets.all(8),
                      decoration: BoxDecoration(color: Colors.blueGrey[300]),
                      child: Column(
                        children: [
                          Container(
                              height: 60,
                              width: 60,
                              child: SizedBox(
                                  child: Image.network(items[7].profileUrl))),
                          Text(items[7].name),
                          Text(items[7].price.toString()),
                          SizedBox(
                            height: 8,
                          ),
                          Material(
                            elevation: 5.0,
                            color: Colors.blue.shade200,
                            child: MaterialButton(
                              minWidth: 20,
                              padding: EdgeInsets.only(
                                  top: 10, bottom: 10, left: 30, right: 30),
                              onPressed: _tambahhargaimc,
                              child: Text(
                                "Beli",
                                textAlign: TextAlign.center,
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),

            //#soal 3 silahkan buat coding disini
            //untuk container boleh di pake/dimodifikasi
            Container(
              height: MediaQuery.of(context).size.height / 1.5,
              //child: //silahkan dilanjutkan disini
            ),

            //sampai disini soal 3
          ],
        ),
      ),
    ));
  }
}

Widget myWidget(BuildContext context) {
  return MediaQuery.removePadding(
    context: context,
    removeTop: true,
    child: GridView.builder(
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
        ),
        itemCount: 300,
        itemBuilder: (BuildContext context, int index) {
          return Card(
            color: Colors.amber,
            child: Center(child: Text('$index')),
          );
        }),
  );
}
