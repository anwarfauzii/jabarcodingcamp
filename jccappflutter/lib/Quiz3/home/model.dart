class ChartModel {
  final String name;
  final int price;
  final String profileUrl;

  ChartModel(
      {required this.name, required this.price, required this.profileUrl});
}

final List<ChartModel> items = [
  ChartModel(
      name: 'Komputer Gaming',
      price: 20000000,
      profileUrl:
          'https://cf.shopee.co.id/file/b0be6776bfeaa57d1638c03aa9782716'),
  ChartModel(
      name: 'I Mac ',
      price: 25000000,
      profileUrl:
          "https://s0.bukalapak.com/uploads/attachment/37435/mac-feature-image.jpg"),
  ChartModel(
      name: 'Komputer Gaming',
      price: 20000000,
      profileUrl:
          'https://cf.shopee.co.id/file/b0be6776bfeaa57d1638c03aa9782716'),
  ChartModel(
      name: 'I Mac ',
      price: 25000000,
      profileUrl:
          "https://s0.bukalapak.com/uploads/attachment/37435/mac-feature-image.jpg"),
  ChartModel(
      name: 'Komupter Gaming',
      price: 20000000,
      profileUrl:
          'https://cf.shopee.co.id/file/b0be6776bfeaa57d1638c03aa9782716'),
  ChartModel(
      name: 'I Mac ',
      price: 25000000,
      profileUrl:
          "https://s0.bukalapak.com/uploads/attachment/37435/mac-feature-image.jpg"),
  ChartModel(
      name: 'Komupter Gaming',
      price: 20000000,
      profileUrl:
          'https://cf.shopee.co.id/file/b0be6776bfeaa57d1638c03aa9782716'),
  ChartModel(
      name: 'I Mac ',
      price: 25000000,
      profileUrl:
          "https://s0.bukalapak.com/uploads/attachment/37435/mac-feature-image.jpg"),
];
