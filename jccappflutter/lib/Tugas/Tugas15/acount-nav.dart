import 'package:flutter/material.dart';
class Account extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text("Ini Adalah Halaman Account !!!", style: TextStyle(fontSize: 24),),
      ),
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        notchMargin: 10,
        child: Container(
          height: 60,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween, 
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                
                children: [
                  MaterialButton(
                    minWidth: 160,
                    onPressed: (){
                      Navigator.pushNamed(context, '/home');
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.home,
                          color: Colors.blue,
                        ),
                        Text(
                          'Home', 
                          style: TextStyle(color: Colors.black),
                        )
                      ],),
                    ),

                  MaterialButton(
                    minWidth: 150,
                    onPressed: (){
                      Navigator.pushNamed(context, '/search');
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.search,
                          color: Colors.blue,
                        ),
                        Text(
                          'Search', 
                          style: TextStyle(color: Colors.black),
                        )
                      ],),
                    ),
                  MaterialButton(
                    minWidth: 160,
                    onPressed: (){
                      Navigator.pushNamed(context, '/account');
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.account_circle,
                          color: Colors.blue,
                        ),
                        Text(
                          'Account', 
                          style: TextStyle(color: Colors.black),
                        )
                      ],),
                    ),                  
                ],
              )
            ],),
        ),),

    );
  }
}