import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jccappflutter/Tugas/Tugas15/DrawerScreen.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(       
      title: Text("Home"),
      ),
      drawer: DrawerScreen(),
      body: SingleChildScrollView(child: 
      
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 42.84),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 66),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                IconButton(icon: Icon(Icons.notifications), onPressed: (){},),
                IconButton(icon: Icon(Icons.add_shopping_cart), onPressed: (){},),
                IconButton(icon: Icon(Icons.logout), onPressed: (){Navigator.pushNamed(context, '/');})
              ],
              ),
              SizedBox(height: 10),
              Text.rich(TextSpan(
                text: 'Welcome,',style: TextStyle(fontSize: 40,color: Colors.blue[300],
                fontWeight: FontWeight.bold, fontFamily: 'Poppins'),
              ),
              style:  TextStyle(fontSize: 30),
              ),
              Text.rich(TextSpan(
                 text: 'Anwar', style: TextStyle(fontWeight: FontWeight.w400, 
                 fontSize: 38, fontFamily: 'Poppins', color: Colors.blue[900])
              )
              ),
              SizedBox(height: 68),
              TextField(decoration: InputDecoration(
                prefixIcon: Icon(Icons.search, size: 18, color: Colors.black),
                border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.blue.shade200, width: 10.0),
                  borderRadius: BorderRadius.circular(8),
                  
                ),
                hintText: 'Search',
              ),
            ),
              SizedBox(height: 72),
              Text('Recomended Places', 
              style: TextStyle(fontWeight: FontWeight.normal, fontSize: 14, fontFamily: 'Poppins')
              ),
              SizedBox(height: 15),
              SizedBox(
                height: 250,
                child: GridView.count(
                  padding: EdgeInsets.zero,
                  crossAxisCount: 2,
                  childAspectRatio: 1.491,
                  crossAxisSpacing: 21,
                  mainAxisSpacing: 14,
                  physics: NeverScrollableScrollPhysics(),
                  children: [
                    for(var country in countries)
                    Image.asset('assets/images/$country.png')
                  ],
                ),
              ),

          ],
        ),
      ),
      ),
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        notchMargin: 10,
        child: Container(
          height: 60,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween, 
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                
                children: [
                  MaterialButton(
                    minWidth: 160,
                    onPressed: (){
                      Navigator.pushNamed(context, '/home');
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.home,
                          color: Colors.blue,
                        ),
                        Text(
                          'Home', 
                          style: TextStyle(color: Colors.black),
                        )
                      ],),
                    ),

                  MaterialButton(
                    minWidth: 150,
                    onPressed: (){
                      Navigator.pushNamed(context, '/search');
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.search,
                          color: Colors.blue,
                        ),
                        Text(
                          'Search', 
                          style: TextStyle(color: Colors.black),
                        )
                      ],),
                    ),
                  MaterialButton(
                    minWidth: 160,
                    onPressed: (){
                      Navigator.pushNamed(context, '/account');
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.account_circle,
                          color: Colors.blue,
                        ),
                        Text(
                          'Account', 
                          style: TextStyle(color: Colors.black),
                        )
                      ],),
                    ),                  
                ],
              )
            ],),
        ),),
      
    );
  }
}

final countries = ['Monas', 'Roma', 'Berlin', 'Tokyo'];