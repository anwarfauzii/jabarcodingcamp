import 'package:flutter/material.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 41.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 49.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text.rich(TextSpan(
                  text: "Sanber Flutter", style: TextStyle(fontSize: 30, fontFamily: 'Poppins',
                  fontWeight: FontWeight.w600, color: Colors.blue.shade200)
                ))
              ], 
            ),
            SizedBox(height: 13),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget> [
                Image.asset('assets/images/flutter.png',height: 100, width: 93.8,)
              ],
            ),
            SizedBox(height: 29,),
            TextField(decoration: InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide(color: Colors.blue.shade200)
                 ),
                 hintText: "Username"
              ),),
              SizedBox(height: 18.5),
              TextField(obscureText: true,decoration: InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide(color: Colors.blue.shade200)
                 ),
                 hintText: "Password"
              ),),
              SizedBox(height: 18.5),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  TextButton(
            style: TextButton.styleFrom(
              textStyle: TextStyle(fontSize: 20),
            ),
            onPressed: () {},
            child: const Text('Forgot Password',style: TextStyle(fontSize: 14, color: Colors.blueAccent, fontFamily: "Poppins"),),
          ),
                ],
              ),
              SizedBox(height: 8),
                  Material(
                    elevation: 5.0,
                    color: Colors.blue.shade200,
                      child: MaterialButton(
                        minWidth: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.only(top: 20, bottom: 20),
                        onPressed: () {},
                        child: Text("Login",
                        textAlign: TextAlign.center,style: TextStyle(color: Colors.white),
                          ),
                    ),
                  ),
                  SizedBox(height: 19),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text.rich(TextSpan(
                        text: "Does not have an account ?",
                        style: TextStyle(fontSize: 14, color: Colors.black, fontFamily: 'Poppins'), 
                      ),
                      ),
                      SizedBox(width: 23),
                      
                      TextButton(
                      style: TextButton.styleFrom(
                        textStyle: TextStyle(fontSize: 20),
                      ),
                      onPressed: () {},
                      child: const Text('Sign In',style: TextStyle(fontSize: 14, color: Colors.blueAccent, fontFamily: "Poppins"),),
                    ),
                    ],
                  )
          ],
        ),
      ),
    );
  }
}