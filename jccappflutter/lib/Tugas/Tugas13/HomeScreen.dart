import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 42.84),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 66),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                IconButton(icon: Icon(Icons.notifications), onPressed: (){},),
                IconButton(icon: Icon(Icons.add_shopping_cart), onPressed: (){},)
              ],
              ),
              SizedBox(height: 10),
              Text.rich(TextSpan(
                text: 'Welcome,',style: TextStyle(fontSize: 40,color: Colors.blue[300],
                fontWeight: FontWeight.bold, fontFamily: 'Poppins'),
              ),
              style:  TextStyle(fontSize: 30),
              ),
              Text.rich(TextSpan(
                 text: 'Anwar', style: TextStyle(fontWeight: FontWeight.w400, 
                 fontSize: 38, fontFamily: 'Poppins', color: Colors.blue[900])
              )
              ),
              SizedBox(height: 68),
              TextField(decoration: InputDecoration(
                prefixIcon: Icon(Icons.search, size: 18, color: Colors.black),
                border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.blue.shade200, width: 10.0),
                  borderRadius: BorderRadius.circular(8),
                  
                ),
                hintText: 'Search',
              ),
            ),
              SizedBox(height: 72),
              Text('Recomended Places', 
              style: TextStyle(fontWeight: FontWeight.normal, fontSize: 14, fontFamily: 'Poppins')
              ),
              SizedBox(height: 15),
              SizedBox(
                height: 250,
                child: GridView.count(
                  padding: EdgeInsets.zero,
                  crossAxisCount: 2,
                  childAspectRatio: 1.491,
                  crossAxisSpacing: 21,
                  mainAxisSpacing: 14,
                  physics: NeverScrollableScrollPhysics(),
                  children: [
                    for(var country in countries)
                    Image.asset('assets/images/$country.png')
                  ],
                ),
              )
          ],
        ),
      ),
      
    );
  }
}

final countries = ['Monas', 'Roma', 'Berlin', 'Tokyo'];