import 'package:flutter/material.dart';
import 'package:jccappflutter/Quiz3/home/home_screen.dart';
import 'package:jccappflutter/Quiz3/login/login_screen.dart';


void main()=>runApp(MyApp());
class MyApp extends StatefulWidget {
  const MyApp({ Key? key }) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
   
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Anwar Flutter',
      theme: ThemeData(
        primarySwatch: Colors.blue), 
      home: LoginScreen()
        
    );
  }
}