/*
TUGAS SOAL NO. 1 LOOPING MEMAKAI WHILE

void main(){
  var looping = 2;
  print("++++++++++ LOOPING PERTAMA ++++++++++");
  while(looping <=20){
    print("${looping.toString()} - I Love Coding");
    looping +=2;
  }
  print("++++++++++ LOOPING KEDUA ++++++++++");
  looping = 20;
  while(looping>=2){
    print("${looping.toString()} - I will become a mobile developer");
    looping -=2;
  }
}   */

/* 
TUGAS SOAL NO. 2 LOOPING MENGGUNAKAN FOR

void main (){
  for(var angka = 1; angka<=20; angka++){
    if(angka %3 == 0 && angka %2 == 0){
      print("${angka.toString()} - Berkualitas");
    }else if(angka %3 == 0){
      print("${angka.toString()} - I Love Coding");
    }else if(angka %2 == 1){
      print("${angka.toString()} - Santai");
    }else if(angka %2 == 0){
      print("${angka.toString()} - Berkualitas");
    }
  }
} */

import 'dart:io';

/* TUGAS SOAL NO. 3 MEMBUAT PERSEGI PANJANG #

void main(){
  int bawah = 4;
  int panjang = 8;
  for(int i = 0; i<bawah; i++){
    for(int j = 0; j < panjang; j++){
      stdout.write("#");
    }
    stdout.writeln("");
  }
}
*/

/* TUGAS SOAL NO.4 MEMBUAT TANGGA

void main(){
  int tangga = 7;
  for(int i = 0; i<tangga; i++){
    for(int j = 0; j <= i; j++){
      stdout.write("#");
    }
    stdout.writeln("");
  }
}
*/