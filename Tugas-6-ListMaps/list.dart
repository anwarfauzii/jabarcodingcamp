/*Soal no. 1 Range

import 'dart:io';
void main(){
  print("++++ Program Pengurutan Angka ++++");print(" ");
  stdout.write("Masukan Angka Pertama : ");
  var angka = stdin.readLineSync();
  var angkaInt = int.parse("${angka}");
  assert(angkaInt is int);

  stdout.write("Masukan Angka kedua : ");
  var angka2 = stdin.readLineSync();
  var angka2Int = int.parse("${angka2}");
  assert(angka2Int is int);

  print("");
  print(range(angkaInt, angka2Int));
}
range(startNum, finishNum){
  if(startNum<finishNum){
    List list = [for(; startNum<=finishNum; startNum++)startNum];
    return(list);
  }else{
    List list = [for(; startNum>=finishNum; startNum--)startNum];
    return (list);
  }
}*/

/*Soal no.2 Range With Step
import 'dart:io';
void main(){
   print("++++ Program Pengurutan Angka Dengan Menentukan Jarak Loncatan ++++");print(" ");
  stdout.write("Masukan Angka Pertama : ");
  var angka = stdin.readLineSync();
  var startNum = int.parse("${angka}");
  assert(startNum is int);

  stdout.write("Masukan Angka kedua : ");
  var angka2 = stdin.readLineSync();
  var finishNum = int.parse("${angka2}");
  assert(finishNum is int);

  stdout.write("Masukan Angka Loncatan yang anda inginkan : ");
  var loncat = stdin.readLineSync();
  var step = int.parse("${loncat}");
  assert(step is int);

  print("");
  print(rangeWithStep(startNum,finishNum,step));
}
rangeWithStep(startNum, finishNum, step){
  if(startNum<finishNum){
    List list = [for(; startNum<=finishNum; startNum+=step)startNum];
    return(list);
  }else{
    List list = [for(; startNum>=finishNum; startNum-=step)startNum];
    return (list);
  }
}*/


/*Soal no. 3 List MultiDimensi
void main(){
  var input =[
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/06/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
  ];

print("++++ Program Penampilan Data ++++");print("");
return dataHandling(input);
}
dataHandling(input){

  var datalength = input.length;
  for(var i = 0; i<datalength; i++){
    var Id = 'Nomor ID : ' + input[i][0];
    var nama = 'Nama Lengkap : ' + input[i][1];
    var ttl = 'TTL : ' + input[i][2]+' '+input[i][3];
    var hobi = 'Hobi : ' + input[i][4];
    print(Id);
    print(nama);
    print(ttl);
    print(hobi);
    print(" ");
  };
}*/


/* Soal no.4 Program Balik Kata
void main(){
print(balikkata("Kasur"));
print(balikkata("SanberCode"));
print(balikkata("Haji"));
print(balikkata("rececar"));
print(balikkata("Sanbers"));
}

balikkata(String s){
String kata = "";
for(int i = s.length -1; i >= 0; i--){
  kata += s[i];
}
return kata;
}*/