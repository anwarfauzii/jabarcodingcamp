import 'armor_titan.dart';
import 'attack_titan.dart';
import 'beast_titan.dart';
import 'human.dart';

void main(){
  armorTitan armort = new armorTitan();
  attackTitan attackt = new attackTitan();
  beastTitan beastt = new beastTitan();
  Human human = new Human();

  armort.powerPoint = 3;
  attackt.powerPoint = 8;
  beastt.powerPoint = 9;
  human.powerPoint = 10;

  print("Armor Power : ${armort.powerPoint}");
  print("Attack Power : ${attackt.powerPoint}");
  print("Beast Power : ${beastt.powerPoint}");
  print("Human Power : ${human.powerPoint}");
  print("");
  print("Suara Armor Titan : ${armort.terjang()}");
  print("Suara Attack Titan : ${attackt.punch()}");
  print("Suara Beast Titan : ${beastt.lempar()}");
  print("Suara Human : ${human.killAlltitan()}");
}