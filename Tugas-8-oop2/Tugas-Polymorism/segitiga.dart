import 'bangun_datar.dart';

class segitiga extends bangun_datar{
  late double alas;
  late double tinggi;
  late double s_miring;

  segitiga(double alas, tinggi, s_miring){
    
    this.alas = alas;
    this.tinggi = tinggi;
    this.s_miring = s_miring;

  }
  @override
  double hitungluas(){
    return alas*tinggi/2;
  }
  double hitungkeliling(){
    return alas+tinggi+s_miring;
  }
}