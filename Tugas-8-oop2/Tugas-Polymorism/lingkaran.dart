import 'bangun_datar.dart';

class lingkaran extends bangun_datar{
  double jari2 = 1;

  lingkaran (double jari2){
    this.jari2 = jari2;
  }
  @override
  double hitungluas(){
    return 3.14 * jari2 * jari2;
  }
  double hitungkeliling(){
    return 2*3.14*jari2;
  }
}