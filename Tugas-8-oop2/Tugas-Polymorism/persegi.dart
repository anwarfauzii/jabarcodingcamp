import 'bangun_datar.dart';

class Persegi extends bangun_datar{
  double sisi =1;
  
  Persegi(double sisi){
    this.sisi = sisi;
  }
  @override
  double hitungluas(){
    return sisi * sisi;
  }
  double hitungkeliling(){
    return 4*sisi;
  }
}