import 'bangun_datar.dart';
import 'lingkaran.dart';
import 'persegi.dart';
import 'segitiga.dart';

void main(){
  bangun_datar bd  = new bangun_datar();
  lingkaran lingkar = new lingkaran(7.0);
  Persegi persegi = new Persegi(4.0);
  segitiga segi3 = new segitiga(4.0, 7.0, 10.0);
  bd.hitungluas();
  print("Luas Lingkaran : ${lingkar.hitungluas()}");
  print("Keliling Lingkaran : ${lingkar.hitungkeliling()}");
  bd.hitungkeliling();
  print("Luas Persegi : ${persegi.hitungluas()}");
  print("Keliling Persegi : ${persegi.hitungkeliling()}");
  bd.hitungluas();
  print("Luas Segitiga : ${segi3.hitungluas()}");
  print("Keliling Segitiga : ${segi3.hitungkeliling()}");

}