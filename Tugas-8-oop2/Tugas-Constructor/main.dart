import 'employee.dart';
import 'dart:io';
void main(List<String> args) {
  print("++++ Program Constructor ++++");
  stdout.write("Masukan Nomor Identitas Anda : ");
  String? id = stdin.readLineSync();
  if(id?.isEmpty??true){
    print("Anda Harus Mengisi ID !!!");
    }else{
  var idd = int.parse('${id}');
  assert(idd is int);
  stdout.write("Masukan Nama Anda : ");
  String? name = stdin.readLineSync();
  stdout.write("Masukan Nama Department Anda : ");
  String? department = stdin.readLineSync();
  var data1 = new Employee.id(idd); //nomor id anda
  var data2 = new Employee.name('${name}'); //nama anda, contoh Anwar Fauzi
  var data3 = new Employee.department('${department}');
  
  print("");
  print("Nomor Id Anda : ${data1.id}");
  print("Nama Anda : ${data2.name}");
  print("Nama Department Anda : ${data3.department}");
  }
}