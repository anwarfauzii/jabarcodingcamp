// import 'dart:io';
// Soal no. 1 Ternary Operator

// void main(){
//     print("Apakah anda akan melanjutkan penginstalan (Y/T)");
//   var pilihan = stdin.readLineSync();
//     pilihan=="Y"||pilihan=="y" ? print("Anda Akan Menginstal Aplikasi Dart") : print("Aborted");
// }


// Soal No. 2 If-Else if dan Else

// void main(){
//   print("========== PERMAINAN WAREWOLF ==========");print(" ");
//   print("Terdapat Tiga Peran untuk dipilih : Penyihir, Guard, Werewolf");print(" ");
//   print("Silahkan Masukan Nama Anda : ");
//   var nama = stdin.readLineSync();
  
//   if(nama == "" || nama == " "){
//     print("Nama Harus diisi !");
//   }else{
//     print("Silahkan Masukan Peran yang anda inginkan :");
//     var peran = stdin.readLineSync();

//     if(peran == "" || peran == " "){
//       print("Halo ${nama}, Kamu harus memilih Peran untuk memulai game !");
//     }else if(peran == "Penyihir" || peran == "penyihir"){
//       print("Selamat datang di dunia Werewolf, ${nama}");
//       print("Halo Penyihir ${nama}, kamu dapat melihat siapa yang menjadi werewolf !");
//     }else if(peran == "Guard" || peran == "guard"){
//       print("Selamat datang di dunia Werewolf, ${nama}");
//       print("Halo Guard ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf !");
//     }else if(peran == "Werewolf" || peran == "werewolf"){
//       print("Selamat datang di dunia Werewolf, ${nama}");
//       print("Halo Werewolf ${nama}, kamu akan memakan mangsa setiap malam !");
//     }else{
//       print("Tidak Ada pilihan Role yang anda inginkan");
//     }
//   }
// }


//Tugas soal no.3 Pilihan Quotes

// void main(){
//   print("Program Quotes Sehari hari");
//   print("++++++++++++++++++++++++++");print("  ");
//   print("Sebutkan hari untuk melihat Quotes !");
//   stdout.write('Mau lihat Quotes hari apa ? ');
//   String? hari = stdin.readLineSync();
//   switch (hari) {
//     case 'Senin': {print("Segala sesuatu memiliki kesudahan, yang sudah berakhir biarlah berlalu dan yakinlah semua akan baik-baik saja.");break;}
//     case 'senin': {print("Segala sesuatu memiliki kesudahan, yang sudah berakhir biarlah berlalu dan yakinlah semua akan baik-baik saja.");break;}
//     case 'Selasa': {print("Setiap detik sangatlah berharga karena waktu mengetahui banyak hal, termasuk rahasia hati.");break;}
//     case 'selasa': {print("Setiap detik sangatlah berharga karena waktu mengetahui banyak hal, termasuk rahasia hati.");break;}
//     case 'Rabu': {print("Jika kamu tak menemukan buku yang kamu cari di rak, maka tulislah sendiri");break;}
//     case 'rabu': {print("Jika kamu tak menemukan buku yang kamu cari di rak, maka tulislah sendiri");break;}
//     case 'Kamis': {print("Jika hatimu banyak merasakan sakit, maka belajarlah dari rasa sakit itu untuk tidak memberikan rasa sakit pada orang lain.");break;}
//     case 'kamis': {print("Jika hatimu banyak merasakan sakit, maka belajarlah dari rasa sakit itu untuk tidak memberikan rasa sakit pada orang lain.");break;}
//     case 'Jumat': {print("Hidup tak selamanya tentang pacar.");break;}
//     case 'jumat': {print("Hidup tak selamanya tentang pacar.");break;}
//     case 'Sabtu': {print("Rumah bukan hanya sebuah tempat, tetapi itu adalah perasaan.");break;}
//     case 'sabtu': {print("Rumah bukan hanya sebuah tempat, tetapi itu adalah perasaan.");break;}
//     case 'Minggu': {print("Hanya seorang yang takut yang bisa bertindak berani. Tanpa rasa takut itu tidak ada apapun yang bisa disebut berani.");break;}
//     case 'minggu': {print("Hanya seorang yang takut yang bisa bertindak berani. Tanpa rasa takut itu tidak ada apapun yang bisa disebut berani.");break;}
//     default:{print("Hari yang anda inginkan tidak terdaftar, Harap perhatikan penulisan hari.");}
//   }
// }


//Tugas soal no.4 Switch Case Hari Bulan Tahun

// void main(){
//   int hari = 21;
//   int bulan = 1;
//   int tahun = 1945;
//   if(hari >= 1 && hari <=31){
//     if(bulan >=1 && bulan <=12){
//       if(tahun>=1900 && tahun <=2200){
//   switch (bulan) {
//     case 1:{print("${hari} Januari ${tahun}");break;}
//     case 2:{print("${hari} Februari ${tahun}");break;}
//     case 3:{print("${hari} Maret ${tahun}");break;}
//     case 4:{print("${hari} April ${tahun}");break;}
//     case 5:{print("${hari} Mei ${tahun}");break;}
//     case 6:{print("${hari} Juni ${tahun}");break;}
//     case 7:{print("${hari} Juli ${tahun}");break;}
//     case 8:{print("${hari} Agustus ${tahun}");break;}
//     case 9:{print("${hari} September ${tahun}");break;}
//     case 10:{print("${hari} Oktober ${tahun}");break;}
//     case 11:{print("${hari} November ${tahun}");break;}
//     case 12:{print("${hari} Desember ${tahun}");break;}
//     default: {print("Tidak Terdaftar");}
//   }}else{
//     print("Tahun hanya bisa diisi dari tahun 1900 sampai 2200");
//   }}else{
//     print("Bulan hanya bisa diisi dari bulan 1 sampai 12");
//   }}else{
//     print("Hari hanya bisa diisi dari angka 1 sampai 31");
//   }
// }