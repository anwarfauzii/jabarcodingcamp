import 'segitiga.dart';
import 'dart:io';
void main(){
  Segitiga segitiga;
  double luassegitiga;
  segitiga = new Segitiga();
  
  print("++++ Program Perhitungan Luas Segitiga ++++");print(" ");
  stdout.write("Masukan Panjang Alas Segitiga : ");
  var p_alas = stdin.readLineSync();
  double p_alas2 = double.parse('${p_alas}');
  
  stdout.write("Masukan Tinggi Segitiga : ");
  var p_tinggi = stdin.readLineSync();
  double p_tinggi2 = double.parse('${p_tinggi}');

  segitiga.alas = p_alas2;
  segitiga.tinggi = p_tinggi2;
  luassegitiga = segitiga.luas;
  print("");
  print("Luas Segitiganya adalah : ${luassegitiga}");
}