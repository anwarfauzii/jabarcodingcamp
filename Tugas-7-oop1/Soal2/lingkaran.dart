class LuasLingkaran{
  double _jari2 = 1;
  void setjari2(double value){
    if (value < 0){
      value *= -1;
    }
    _jari2 = value;
  }
  double getjari2(){
    return _jari2;
  }
  double hitungluas(){
    return 3.14 * _jari2 * _jari2;
  }
}